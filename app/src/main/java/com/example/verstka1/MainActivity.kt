package com.example.verstka1

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {
    lateinit var text_hz: TextView
    lateinit var username_enter: EditText
    lateinit var text_date: TextView
    lateinit var calendar: CalendarView
    lateinit var rolik: VideoView
    private var kontroller: MediaController? = null // говорим kontroller, что он является наследником MediaController
    lateinit var check1: CheckBox
    lateinit var reg_layout: LinearLayout
    lateinit var enter_phone: EditText
    lateinit var enter_pass: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        text_hz = findViewById(R.id.text_hz)
        username_enter = findViewById(R.id.username_enter)
        text_date = findViewById(R.id.movieday)
        rolik = findViewById(R.id.video)
        check1 = findViewById(R.id.checkboxid)
        enter_phone = findViewById(R.id.phone_enter)
        enter_pass = findViewById(R.id.pass_enter)
        chek()
        reg_layout = findViewById(R.id.layout_reg)
        //val uri: String = "http://www.cats.com/cat-speak.3gp"
        //rolik.setVideoURI(Uri.parse(uri))
        rolik.setVideoPath("android.resource://" + getPackageName() + "/" + R.raw.videoplayback)
        //rolik.setVideoPath("R.raw.videoplayback")
        kontroller = MediaController(this) // говорим kontroller, что он выполняет роль MediaController в этом файле или активити.
        kontroller?.setAnchorView(rolik) // хз, что-то с якорем)
        rolik.setMediaController(kontroller) // применяем к видосу медиаконтроллер, конкретный наш kontroller
        // kontroller?.setAnchorView(rolik2)   ?
        // rolik2.setMediaController(kontroller)   ?
        calendar = findViewById(R.id.calendarView)
        calendar.setOnDateChangeListener{view, year, month, day ->
            text_date.text = "$day.${month+1}.$year"
        }

    }
    fun chek(){
        check1.setOnCheckedChangeListener{buttonView, isChecked ->
            if(isChecked){
                reg_layout.visibility = View.VISIBLE
            }
            else{
                reg_layout.visibility = View.GONE
            }
        }
    }

    fun save(view: android.view.View) {
            if (username_enter.text.toString().isNotEmpty()){
               val name = username_enter.text.toString()
               text_hz.text = "Добро пожаловать, $name!"
               text_hz.visibility = View.VISIBLE

        }
    }

    fun send(view: android.view.View) {
           if(enter_phone.text.toString().isNotEmpty() && enter_pass.text.toString().isNotEmpty()){
               Toast.makeText(this, "Вы успешно зарегистрированы", Toast.LENGTH_LONG).show()
           }
           else{
               Toast.makeText(this, "Заполните все поля", Toast.LENGTH_SHORT).show()
           }
    }

}